const fsPromises = require("fs/promises");
const fs = require("fs");
const path = require("path");

function deleteJSONFiles(fileIndex, fileArray, directoryName) {

    return new Promise((resolve, reject) => {
                ``
        if (fileIndex < fileArray.length) {
            fs.unlink(path.resolve(directoryName, `${fileArray[fileIndex]}`), function (errorWhileDeletingFiles) {
                if (errorWhileDeletingFiles) {
                    console.error(`Error while Deleting ${fileArray[fileIndex]} --> deleteFiles function --> deleteJSONFiles function --> fs.unlink function --> if block.`)
                    reject(errorWhileDeletingFiles);
                }
                else {
                    console.log(`${fileArray[fileIndex]} deleted successfully.`);
                        
                    deleteJSONFiles(fileIndex + 1, fileArray, directoryName)
                        .then((successMessage) => {
                            resolve(successMessage);
                        })
                        .catch((errorWhileDeletingFiles) => {
                            reject(errorWhileDeletingFiles);
                        });
                    
                }
            });
        }
        else {
            resolve(`${fileArray.length}`);
        }
    });
}

function deleteFiles(fileArray, directoryName) {

    return new Promise((resolve, reject) => {

        // Check if directory exist
        fsPromises.readdir(path.resolve(directoryName))
            .then(() => {
                // Call delete function with index 0;
                return deleteJSONFiles(0, fileArray, directoryName)
            })
            .then((successMessage) => {
                resolve(successMessage);
            })
            .catch((errorWhileDeletingFiles) => {
                reject(errorWhileDeletingFiles);
            });
            
        
        
   
    });
}


module.exports = deleteFiles;
