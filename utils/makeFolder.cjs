
const fs = require("fs");
const path = require("path");


function makeFolder(directoryName) {
    return new Promise((resolve, reject) => {
        fs.mkdir(path.resolve(directoryName), (errorCreatingDirectory) => {
            if (errorCreatingDirectory) {
                console.error("Error creating Directory --> makeFolder function --> if block");
                reject(errorCreatingDirectory);
            }
            else {
                resolve(`Successfully created a directory named ${directoryName}.`);
            }
        });
    });
}


module.exports = makeFolder;