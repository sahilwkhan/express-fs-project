


function inputValidation(receivedData) {

    // Check directory name data type
    if (typeof receivedData.directory !== 'string' && typeof receivedData.directory !== 'number') {
        const directoryDataTypeError = {
            source: "InputValidation function --> first if block",
            message: "Directory name must be a non-empty string or number.",
            code: 400
        }
        return directoryDataTypeError;
    }
    else if (typeof receivedData.directory === 'string' && receivedData.directory.indexOf("/") > -1) {
        // Check for '/' in directory named
        const nameWithInvalidCharacter = {
            source: "InputValidation function --> first else-if block",
            message: "Directory name cannot have '/'.",
            code: 400
        }
        return nameWithInvalidCharacter;
    }
    else if (String(receivedData.directory).trim().length == 0) {
        // Check directory name validity
        const emptyDirectoryName = {
            source: "InputValidation function --> second else-if block",
            message: "Directory name cannot be an empty string.",
            code: 400
        }
        return emptyDirectoryName;
    }
    else if (receivedData.listOfFiles == undefined || !Array.isArray(receivedData.listOfFiles)) {
        // Check list of files for Array data type
        const invalidListDataType = {
            source: "InputValidation function --> third else-if block",
            message: "List of file names should be an array.",
            code: 400
        }
        return invalidListDataType;
    }
    else if (receivedData.listOfFiles.length === 0) {
        // Check if list of files is empty
        const emptyListOfFiles = {
            source: "InputValidation function --> fourth else-if block",
            message: "List of file names cannot be empty.",
            code: 400
        };
        return emptyListOfFiles;
    }
    else {
        return "OK";
    }
}


module.exports = inputValidation;