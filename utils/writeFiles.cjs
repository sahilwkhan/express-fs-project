
const fsPromises = require("fs/promises");
const fs = require("fs");
const path = require("path");


function writeFiles(fileArray, directoryName, BodyFileArray) {
    
    return new Promise((resolve, reject) => {

        const fileNamePromises = fileArray.map((fileToCreate) => {
            let invalidFileNameType;
            if (typeof fileToCreate !== "string") {
                if (Array.isArray(fileToCreate)) {
                    invalidFileNameType = `[${fileToCreate}]`;
                }
                else if (typeof fileToCreate === "object") {
                    invalidFileNameType = `${fileToCreate}`;
                }
                else {
                    invalidFileNameType = fileToCreate;
                }
                const nonStringFileName = {
                    source: "Error in postRoute --> writeFiles -->  fileArray.map() -- >if block",
                    message: `Cannot create ${invalidFileNameType}. File name must be of string data type.`,
                    code: 400
                }
                reject(nonStringFileName);
            }
            else if (fileToCreate.length === 0) {
                const emptyStringFileName = {
                    source: "Error in postRoute --> writeFiles --> --> fileArray.map() -- > else if block",
                    message: `Cannot create file no. ${BodyFileArray.indexOf(fileToCreate)+1}. File name cannot be empty.`,
                    code: 400
                }
                reject(emptyStringFileName);
            }
            else {
                const fileData = {
                    FileName: fileToCreate,
                    Data: `This is ${BodyFileArray.indexOf(fileToCreate) + 1 } file created by writeFiles function using fs/promises writeFile function.`
                }

                // new Promise
                fs.writeFile(path.resolve(directoryName, fileToCreate), JSON.stringify(fileData), (errorWritingFiles) => {
                    if (errorWritingFiles) {
                        const errorWritingFileName = {
                            source: "Error in postRoute --> writeFiles --> fileArray.map() -- > else block --> fs.writeFile function --> if block ",
                            message: `Unable to create ${fileToCreate} file.`,
                            code: 400
                        }
                        return reject(errorWritingFileName);
                    }
                    else {
                        return resolve(`${fileToCreate} created Successfully`);
                    }
                });
            };
        });


        Promise.allSettled(fileNamePromises)
            .then(() => {
                return resolve(fileArray);
            })
            .catch((errorWritingFiles) => {
                console.log("Promise all settle catch :-");
                return reject(errorWritingFiles);
            });
        
    });
};


module.exports = writeFiles;