const express = require("express");;
const routePostPath = require("./routes/postRoute.cjs");
const routeDeletePath = require("./routes/deleteRoute.cjs");
const defaultErrorHandler = require("./errorHandlerFunction/defaultErrorHandler.cjs")

const app = express();
const PORT = process.env.PORT || 4000;


// Body Parser
app.use(express.json());

app.use("/createFiles", routePostPath);
app.use("/deleteFiles", routeDeletePath);

// Default Error Handle
app.use(defaultErrorHandler);

app.listen(PORT, () => {
  console.log(`Server initialized on port ${PORT}`);
});
  