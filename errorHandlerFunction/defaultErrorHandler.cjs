
function defaultErrorHandler(error, request, response, next) {
    console.error("Error catched by error Handler :-")
    if (error.statusCode === 400) {
        console.error("Invalid JSON request.\n");
        response
            .status(400)
            .json({ message: "Invalid JSON Format" })
            .end();
    }
    else {
        console.log(Object.keys(error));
        console.log(error,"\n");
        response
            .status(error.code)
            .json({ message: error.message })
            .end();
    }
}

module.exports = defaultErrorHandler;