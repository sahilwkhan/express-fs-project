const express = require("express");
const router = express.Router();


const deleteFiles = require("../utils/deleteFiles.cjs");
const inputValidation = require("../utils/inputValidation.cjs");

router.delete("/", (request, response, next) => {

    const receivedData = {
        directory: request.body.name,
        listOfFiles: request.body.list,
    };

    if (!request.is("application/json")) {
        const nonJsonRequestForDelete = {
            source: "Error in deleteRoute function --> first if block",
            message: "Only JSON request format accepted for DELETE request.",
            code: 400
        }
        next(nonJsonRequestForDelete);
    }

    deleteInputValidCheck = inputValidation(receivedData);
    
    if (deleteInputValidCheck == "OK") {
        let folderNameToBeDeleted = String(receivedData.directory);
        folderNameToBeDeleted = folderNameToBeDeleted.trim();
        let deleteFileArray = receivedData.listOfFiles;

        deleteFiles(deleteFileArray, folderNameToBeDeleted)
            .then((deleteFilesMessage) => {
                console.log(`${deleteFilesMessage} files deleted successfully.\n`);
                response.status(200).json({
                    message: `${deleteFilesMessage} files deleted inside ${folderNameToBeDeleted} directory.`
                }).end();
            })
            .catch((error) => {
                console.error("Catched error in deleteRoute : -");
                if (error.code == "ENOENT") {
                    const fileNotExistError = {
                        source: "DeleteRoute function --> catched block --> if block",
                        message: "Directory or files does not exists.",
                        code: 400
                    };
                    next(fileNotExistError);
                }
                else {
                    console.error(error, "\n");
                    response.status(500).json({
                        message: "Internal Server Error"
                    }).end();
                }
            });
    }
    else {
        console.error("Input given for delete request body is not valid :");
        next(deleteInputValidCheck);
    }
});

module.exports = router;