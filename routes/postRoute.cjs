
const fsPromises = require("fs/promises");
const path = require("path");
const express = require("express");
const router = express.Router()


const inputValidation = require("../utils/inputValidation.cjs");
const makeFolder = require("../utils/makeFolder.cjs");
const writeFiles = require("../utils/writeFiles.cjs");


router.post("/", (request, response, next) => {

    if (!request.is("json")) {
        const nonJsonRequestForPost = {
            source: "Error in postRoute function --> first if block",
            message: "Only JSON request format accepted for POST request.",
            code: 400
        }
        next(nonJsonRequestForPost);
    };

    const receivedData = {
        directory: request.body.name,
        listOfFiles: request.body.list,
    };

    const inputCheck = inputValidation(receivedData);

    if (inputCheck == "OK") {

        let folderName = String(receivedData.directory).trim();
        let fileArray = receivedData.listOfFiles;

        fsPromises.readdir(path.resolve(folderName))
            .then((existingFileNames) => {
                const nonExistingFiles = fileArray.filter((newFiles) => {
                    return (!existingFileNames.includes(newFiles));
                });
                return nonExistingFiles;
            })
            .then((filesNotInDirectory) => {
                return writeFiles(filesNotInDirectory, folderName, fileArray);
            })
            .then((filesMessage) => {
                console.log(`"Files Added in ${folderName} : ${filesMessage}\n`);
                if (filesMessage.length == 0) {
                    response.status(200).json({
                        message: `No new files added.`
                    }).end();
                }
                else if (filesMessage.length == 1) {
                    response.status(200).json({
                        message: `One new file, ${filesMessage}  added successfully.`
                    }).end();
                }
                else {
                    response.status(200).json({
                        message: `All ${filesMessage.length} files, ${filesMessage}  added successfully.`
                    }).end();
                }
            })
            .catch((error) => {
                console.error("Catched error in postRoute : -");
                if (error.code == "ENOENT") {
                    makeFolder(folderName)
                        .then((folderMessage) => {
                            console.log(folderMessage);
                            return writeFiles(fileArray, folderName, fileArray);
                        }).then((newFilesMessage) => {
                            console.log("Files created ", newFilesMessage, "\n");
                            let responseMessage;
                            if (newFilesMessage.length === 0) {
                                responseMessage = `No new files created.`
                            }
                            else if (newFilesMessage.length == 1) {
                                responseMessage = `One file, ${newFilesMessage} created.`
                            }
                            else {
                                responseMessage = `All ${newFilesMessage.length} files, ${newFilesMessage} created successfully.`
                            }
                            response.status(200).json({
                                message: responseMessage
                            }).end();
                        })
                        .catch((errorForNewFiles) => {
                            console.error(`Error catched by Post route while creating files in ${folderName} directory : `);
                            if (errorForNewFiles.source) {
                                next(errorForNewFiles);
                            }
                            else {
                                console.error(errorForNewFiles);
                                const unknownFileCreatingError = {
                                    source: "Error while creating files in a new directory in post route.",
                                    message: "Internal Server Error",
                                    code: 500
                                }
                                next(unknownFileCreatingError);
                            };
                        });
                }
                else {
                    console.error("Error catched by Post route while adding files : ");
                    if (error.source) {
                        next(error);
                    }
                    else {
                        console.error(errorForNewFiles)
                        const unknownFileAddingError = {
                            source: "Error while adding files in existing directory in post route.",
                            message: "Internal Server Error",
                            code: 500
                        }
                        next(unknownFileAddingError);
                    };
                };
            });
    }
    else {
        console.error("Input is not valid :");
        next(inputCheck);
    };
});


module.exports = router;